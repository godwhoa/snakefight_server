-- client.lua
inspect = require "inspect"
binser = require 'libs.binser'
require "enet"
local host = enet.host_create()
local server = host:connect("localhost:6789")

local done = false
while not done do
  local event = host:service(0)
  if event then
    if event.type == "connect" then
      print("Connected to", event.peer)
      event.peer:send("test")
      event.peer:send("up")
    elseif event.type == "receive" then
      local data = binser.deserialize(event.data)
      print(inspect(data))
    end
  end
end

server:disconnect()
host:flush()