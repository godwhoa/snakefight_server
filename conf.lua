function love.conf(t)
    t.modules.audio 	= false
    t.modules.joystick 	= false
    t.modules.keyboard 	= false
    t.modules.mouse 	= false
    t.modules.sound 	= false
    t.modules.touch 	= false
    t.modules.video 	= false
    t.modules.window 	= false
end