local Snake = Object:extend()

function Snake:new(len)
	-- for easy access
	self.x,self.y=0,0
	-- direction
	self.dir = ""
	-- segments of snake {{x,y},...}
	self.seg = {{x=love.math.random(ww/sw),y=love.math.random(wh/sh)}}
	-- initial len.
	self:add(len)
	-- bullets
	self.bullets = Bullets()
end

function Snake:add(len)
	for i=1,len do
		local last = {x=self.seg[1].x,y=self.seg[1].y}
		-- it trickles down so we'll just clone the last segment
		table.insert(self.seg,last)
	end
end

function Snake:movement(dt)
	-- trickle down the movement head to tail.
	for i=#self.seg,2,-1 do
		self.seg[i].x = self.seg[i-1].x
		self.seg[i].y = self.seg[i-1].y
	end
	--direction
	if self.dir == "up" then
		self.seg[1].y = self.seg[1].y - 1
	end
	if self.dir == "down" then
		self.seg[1].y = self.seg[1].y + 1
	end
	if self.dir == "left" then
		self.seg[1].x = self.seg[1].x - 1
	end
	if self.dir == "right" then
		self.seg[1].x = self.seg[1].x + 1
	end
	self.x,self.y = math.floor(self.seg[1].x),math.floor(self.seg[1].y)
	--teleport if out of bounds
	-- left edge
	if self.seg[1].x < math.floor(map_left/sw) then
		print("left edge")
		self.seg[1].x = math.floor((map_right+map_x)/sw)
	end
	-- top edge
	if self.seg[1].y < math.floor(map_top/sh) then
		print("top edge")
		self.seg[1].y = math.floor((map_bottom+map_y)/sh)
	end
	-- right edge
	if self.seg[1].x > math.floor(map_right/sw) then
		print("right edge")
		self.seg[1].x = math.floor((map_left)/sw)
	end
	
	-- -- bottom edge
	if self.seg[1].y > math.floor(map_bottom/sh) then
		print("bottom edge")
		self.seg[1].y = math.floor((map_top)/sh)
	end
end

function Snake:shoot()
	self.bullets:add(self.x+(sw/2)/sw,self.y+(sh/2)/sh,self.dir)
end

function Snake:update(dt)
	self.bullets:update(dt)
	self:movement(dt)
	snake_color[2] = (255*dt)
end

-- turns segments and bullets into pack for serialization
function Snake:pack()
	local pack = {
		segments = self.seg,
		bullets = self.bullets.bullets
	}
	return pack
end

return Snake