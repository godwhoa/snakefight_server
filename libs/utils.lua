function printf( ... )
  print(string.format(...))
end

-- wall of shittly named functions
local function round_down(n,m)
	return m*(math.floor(n/m));
end

function nearest_origin(x,y,ww,wh)
	return round_down(x,ww), round_down(y,wh)
end

-- if value is eq. to at least one case it'll return true
-- basically to avoid this:
-- if key == 'up' or key == 'down' or key == 'left' or key == 'right' then
function m_one(value,cases)
	for _,c in ipairs(cases) do
		if c == value then
			return true
		end
	end
	return false
end

-- returns vx,vy for a direction
function v_dir(dir)
	local vx,vy = 0,0
	if dir == "up" then
		vx,vy = 0,-1
	end
	if dir == "down" then
		vx,vy = 0,1
	end
	if dir == "left" then
		vx,vy = -1,0
	end
	if dir == "right" then
		vx,vy = 1,0
	end
	return vx,vy
end