local Hub = Object:extend()

function Hub:new()
	-- has bunch of rooms
	self.rooms = {}
	-- instead of looping thru all rooms to find which room client is in we use this
	self.client_cache = {} 
	-- room count
	self.room_count = 0
end


-- puts client in correct room
-- and caches client_cache[client] = room
function Hub:join(event)
	if self.rooms[event.data] == nil then
		self.rooms[event.data] = Room(event.data)
		self.room_count = self.room_count + 1
	end
	self.rooms[event.data]:join(event)
	self.client_cache[event.peer] = event.data
end

-- checks if client wants to join or broadcast
-- then takes correct action
function Hub:onmessage(event)
	local channel = self.client_cache[event.peer]

	-- if id isn't in cache then it's their first msg.
	-- client uses first msg. to send which channel they wanna join
	if channel ~= nil then
		self.rooms[channel]:onmessage(event)
	else
		self:join(event)
	end
end

function Hub:empty_room(channel)
	-- if room is empty destroy it
	if self.rooms[channel].count == 0 then
		self.rooms[channel] = nil
		self.room_count = self.room_count - 1
	end
end

-- Removes room/client
function Hub:destroy(peer,channel)
	-- remove client from room
	self.rooms[channel]:leave(peer)
	-- remove client from cache
	self.client_cache[peer] = nil
	peer:disconnect_now()
end

function Hub:ondisconnect(event)
	local channel = self.client_cache[event.peer]
	if channel ~= nil then
		self:destroy(event.peer,channel)
	end
end

function Hub:update(dt)
	for channel,room in pairs(self.rooms) do
		self:empty_room(channel)
		room:update(dt)
	end
end

return Hub