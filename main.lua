require "enet"
require "libs.utils"
require "def"
binser = require "libs.binser"
Object = require "libs.classic"
Bullets = require "bullets"
Snake = require "snake"
Room = require "room"
Hub = require "hub"


addr = "0.0.0.0:6789"
host = enet.host_create(addr)
hub = Hub()

print("Server started on "..addr)
function love.update(dt)
	hub:update(dt)
    local event = host:service(0)
	if event then
		if event.type == "connect" then
			hub:join(event)
    	elseif event.type == "receive" then
    		hub:onmessage(event)
    	else
    		hub:ondisconnect(event)
    	end
    end
end