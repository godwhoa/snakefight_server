local Room = Object:extend()

function Room:new(channel)
	-- name of channel
	self.channel = channel
	-- clients[peer] = Snake()
	self.clients = {}
	-- no. of clients
	self.count = 0
	-- timestep
	self.step = 1/10
	self.time = 0
end

function Room:join(event)
	self.clients[event.peer] = Snake(1)
	self.count = self.count + 1
	printf("%s joined %s",event.peer,self.channel)
	print(self.count)
end

function Room:leave(peer)
	printf("%s left %s",peer,self.channel)
	self.clients[peer] = nil
	self.count = self.count - 1
end

function Room:onmessage(event)
	print(event.peer,event.data)
	if m_one(event.data,{"up","down","left","right"}) then
		self.clients[event.peer].dir = event.data
	end
	if event.data == "shoot" then
		self.clients[event.peer]:shoot()
	end
end

function Room:broadcast(msg)
	for c,_ in pairs(self.clients) do
		c:send(msg)
	end
	-- printf("msg: %s channel: %s",msg,self.channel)
end

function Room:broadcast_ex(peer,msg)
	for c,_ in pairs(self.clients) do
		if c ~= peer then
			c:send(msg)
		end
	end
	printf("msgex: %s channel: %s",msg,self.channel)
end

function Room:update(dt)
	self.time = self.time + dt
	if self.time >= self.step then
		self.time = self.time - self.step
		local pack = {}
		for peer,snake in pairs(self.clients) do
			-- update snake
			snake:update(dt)
			-- create data pack to be brodcast
			table.insert(pack,{cid=peer:connect_id(),pos=snake:pack()})
		end
		-- broadcast
		self:broadcast(binser.serialize(pack))
	end
end

return Room